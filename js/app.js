let num = +prompt('Введіть свій вік');

if (isNaN(num)) {
    alert('Введено не число');
} else if (num < 0) {
    alert('Ви ще не народилися');
} else if (num>= 0 && num < 12) {
    alert('Ви дитина');
} else if (num < 18 && num >= 12) {
    alert('Ви підліток');
} else {
    alert('Ви дорослий');
}


const mon = ['січень', 'лютий', 'березень', 'квітень', 'травень',
    'червень', 'липень', 'серпень', 'вересень', 'жовтень', 'листопад', 'грудень'];


let whatMon = (prompt('Введіть місяць')).toLowerCase();

switch (whatMon) {
    case mon[0]:
    case mon[2]:
    case mon[4]:
    case mon[6]:
    case mon[7]:
    case mon[9]:
    case mon[11]: {
        console.log('31 день');
        break
    }
    case mon[3]:
    case mon[5]:
    case mon[8]:
    case mon[10]: {
        console.log('30 днів');
         break
    }
    case mon[1]: {
        console.log('28 днів, високосний рік - 29 днів');
        break
    }
    default : {
        console.log('Такого місяця не існує');
        break
    }
}
